# Puppet environment for deploying Foreman

This was written after perfoming some openstack training that focused on packstack.

Packstack is pretty much an example of how not to use puppet and I figured I could 
provide an alternative for deploying foreman and openstack with the same simplicity as 
packstack without confusing students about process they will use no where else.

Its slightly more complicated than packstack, not everthing is in one answer file, but it 
gives people with little familiarity with puppet a simple working example to go off.

On RHEL7 with no subscription add the following repo for ruby selinux
  [rhel-7-server-optional-rpms]
  name = Red Hat Enterprise Linux 7 Server - Optional (RPMs)
  baseurl =  http://mirror.centos.org/centos/7/os/x86_64/
  enabled = 1
  gpgcheck = 0

Also need to run the following on a barebones system, See http://docs.puppetlabs.com/guides/puppetlabs_package_repositories.html
  sudo rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm
  sudo yum install -y git puppet vim rubygem ruby-devel
  sudo gem install r10k


Then as required
  r10k pupptfile install

