#!/bin/bash

#This can be done with an alias, I normally add an alias like this to .bashrc
currDir=`pwd`

puprun() {
 sudo puppet apply --modulepath=$currDir/modules --hiera_config=$currDir/hiera/localconfig.yaml --factpath=$currDir $currDir/manifests/site.pp
}

puprun "$@"


